import React, { useState } from "react";
import Axios from "axios";

import styles from "./LoginForm.module.css";

import LOGO from "../assets/instagram.png";
import FACEBOOK from "../assets/facebook.png";
import APPSTORE from "../assets/downloadApp.png";
import PLAYSTORE from "../assets/downloadPlay.png";

const LoginForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [disabled, setDisabled] = useState(true);
  const [success, setSuccess] = useState(null);

  const handleLogin = async (e) => {
    e.preventDefault();

    await Axios.post("https://insta-clone-backend.onrender.com/user-login", {
      email,
      password,
    })
      .then(() => {
        setSuccess(true);
        setEmail("");
        setPassword("");
      })
      .catch((err) => alert("something went wrong!"));
  };

  return (
    <div className={styles.container}>
      <div className={styles.wrapper}>
        <div className={styles.logo}>
          <img src={LOGO} alt="insta logo" />
        </div>
        <form onSubmit={handleLogin}>
          <input
            onChange={(e) => {
              setEmail(e.target.value);
              setSuccess(false);
              email.length && setDisabled(false);
            }}
            placeholder="Phone number, username or email address"
            type="email"
          />
          <input
            onChange={(e) => {
              setPassword(e.target.value);
              password.length && setDisabled(false);
            }}
            placeholder="Password"
            type="password"
          />
          <button disabled={disabled}>Log in</button>
        </form>

        <div className={styles.lines}>
          <div className={styles.line}></div>
          OR
          <div className={styles.line}></div>
        </div>

        <div className={styles.message}>
          {success && (
            <span style={{ fontSize: "12px", color: "green" }}>
              User login success!
            </span>
          )}
        </div>

        <div className={styles["oauth"]}>
          <span className={styles.fb}>
            <img src={FACEBOOK} alt="facebook" />
            <p>Login with facebook</p>
          </span>

          <span style={{ fontSize: "12px" }}>Fogotten your password?</span>
        </div>
      </div>

      <div className={styles.wrapper}>
        <p style={{ textAlign: "center" }}>
          {" "}
          Don't have an account? <span>Sign up</span>
        </p>
      </div>

      <p style={{ textAlign: "center" }}>Get the app.</p>

      <div className={styles["app-buttons"]}>
        <img src={PLAYSTORE} alt="appstore" />

        <img src={APPSTORE} alt="playstore" />
      </div>
    </div>
  );
};

export default LoginForm;
